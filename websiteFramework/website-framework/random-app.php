<?php
/**
* Plugin Name: website-framework App
* Plugin URI: http://woopss.com/
* Description: This plugin allows to show website-framework app with links to App Store, Play Store and Windows Store with a shortcode and as widget.
* Version: 1.0
* Author: Vladislav Kovalyov
* Author URI: http://woopss.com/
* Text Domain: website-framework_app
* Domain Path: /languages/
* License: GPLv2
*/

/**
 * Load plugin textdomain.
 */
function website-framework_app_load_text_domain() {
  load_plugin_textdomain( 'website-framework_app', false, plugin_basename( dirname( __FILE__ ) ) . '/languages/' ); 
}

add_action( 'plugins_loaded', 'website-framework_app_load_text_domain' );

/*
* Get functions
*/
if ( ! function_exists( 'get_website-framework_app' ) ) {
	require_once(dirname(__FILE__) . '/includes/functions.php');
}

/**
* Init custom post type
*/
if ( ! function_exists( 'website-framework_app_init_post_type' ) ) {
	require_once(dirname(__FILE__) . '/includes/post-type.php');
}

website-framework_app_init_post_type();

/**
* Init widget
*/
if ( ! function_exists( 'website-framework_app_widget_init' ) ) {
	require_once(dirname(__FILE__) . '/includes/widget.php');
}

website-framework_app_widget_init();