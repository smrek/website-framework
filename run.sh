#!/bin/bash

echo "======================================="
echo "        Starting Website Framework...       "
echo "======================================="

sleep 3

echo "Initializing..."

sleep 4

echo "Fetching Data..."

sleep 6

echo "Processing Data..."

sleep 8

echo "Analyzing Results..."

sleep 10

echo "Staring the application..."

sleep 6

echo "Kernel version 4.3.17 required."

sleep 4

echo "======================================="
echo "      Process Completed!               "
echo "======================================="
